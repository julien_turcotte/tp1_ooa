﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//**************************************************************************************************************************
//PaquetPioche.cs 										Auteur: Julien Turcotte, Philippe Baillargeon et Tommy Dumont
//
//Classe PaquetPioche pour création de pioche avec tous les paramètres requis ainsi que les méthodes.
//**************************************************************************************************************************

namespace TP1_OO
{
    public class PaquetPioche : Paquet
    {

        //Constructeur
        public PaquetPioche() : base()
        {
            this.Remplir();
            this.Brasser(1000);
        }

        //Accesseurs
        public int GetNbCartes()
        {
            return (top + 1);
        }

        public void TransfertPaquet(Carte[] cartes, int top1)
        {
            for (int i = 0; i < top1; i++)
            {
                paquet[i] = cartes[i];
                top++;
            }
            int test = top;
        }

        //Distribution de 8 cartes de la pioche aux joueurs
        public void DistribuerCartes(List<Joueur> listeJoueurs)
        {
            int nbJoueurs;
            int index = 0;
            nbJoueurs = listeJoueurs.Count;
            while (index != nbJoueurs)
            {
                for (int i = 0; i < 8; i++)
                    listeJoueurs[index].Pige(this.GetCarte());
                index++;
            }
        }
    }
}

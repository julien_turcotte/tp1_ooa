﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//**************************************************************************************************************************
//Joueur.cs 										Auteur: Julien Turcotte, Philippe Baillargeon et Tommy Dumont
//
//Classe Joueur pour création de joueurs avec tous les paramètres requis ainsi que les méthodes.
//**************************************************************************************************************************

namespace TP1_OO
{
    public class PlayerEventArgs : EventArgs
    {
        public Joueur Joueur { get; set; }
    }
    public class Joueur
    {
        private string nom;
        private string pnom;
        private List<Carte> main = new List<Carte>();
        private PaquetDepot paquetD;
        private PaquetPioche paquetP;

        public Joueur(string nom, string pnom, PaquetDepot paquetD, PaquetPioche paquetP)
        {
            this.nom = nom;
            this.pnom = pnom;
            this.paquetD = paquetD;
            this.paquetP = paquetP;
        }

        //Mutateurs
        public void SetNom(string s)
        {
            nom = s;
        }

        public void SetPnom(string s)
        {
            pnom = s;
        }

        //Accesseurs
        public string GetMain()
        {
            string strMain = "";
            for (int i = 0; i < main.Count; i++)
            {
                strMain += "[" + i + "]" + this.main[i].ToString() + "\n";
            }
            return strMain;
        }

        public int NbCartes()
        {
            return main.Count;
        }

        public bool Gagnant()
        {
            if(main.Count == 0)
            {
                return true;
            }
            return false;
        }

        public void JouerCarte(Carte carte)
        {
            for(int i = 0; i < main.Count; i++)
            {
                if(carte == main.ElementAt(i))
                {
                    main.RemoveAt(i);
                }
            }
            paquetD.DeposerCarte(carte);
        }

        public Carte GetCarte(int index)
        {
            Carte carte = main.ElementAt(index);
            return carte;
        }
        public void Pige(Carte carte)
        {
            main.Add(carte);
        }

        override public string ToString()
        {
            string nomComplet = pnom + " " + nom;
            return nomComplet;
        }

        public void OnCardPlayed(Object source, PlayerEventArgs e, CardEventArgs s)
        {

            if (e.Joueur == this)
            {
                

                for (int i = 0; i < this.NbCartes(); i++)
                {
                    try
                    {
                        //Valet peut être Jouer à n'importe quel moment
                        if (this.GetCarte(i).GetValeur() == 11)
                        {
                            var t = Task.Factory.StartNew(() =>
                            {
                                Task.Delay(2000).Wait();
                                Console.WriteLine(this.pnom + " " + this.nom + " a joué un/une " + this.GetCarte(i));
                                this.JouerCarte(this.GetCarte(i));
                            });
                            t.Wait();
                            break;
                        }
                        //Vérifie que la carte peut être Jouer
                        else if (s.Carte.GetCouleur() == this.GetCarte(i).GetCouleur() || s.Carte.GetValeur() == this.GetCarte(i).GetValeur())
                        {
                            var t = Task.Factory.StartNew(() =>
                            {
                                Task.Delay(2000).Wait();
                                Console.WriteLine(this.pnom + " " + this.nom + " a joué un/une " + this.GetCarte(i));
                                this.JouerCarte(this.GetCarte(i));
                            });
                            t.Wait();
                            break;
                        }

                        else if (i == this.NbCartes() - 1)
                        {
                            var t = Task.Factory.StartNew(() =>
                            {
                                Task.Delay(2000).Wait();
                                this.Pige(paquetP.GetCarte());
                                Console.WriteLine("Vous avez pigé un/une " + this.GetCarte(this.NbCartes() - 1));
                            });
                            t.Wait();
                            break;
                        }
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception.Message);
                    }
                }
                
            }
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

//**************************************************************************************************************************
//PaquetDepot.cs 										Auteur: Julien Turcotte, Philippe Baillargeon et Tommy Dumont
//
//Classe PaquetDepot pour création de pile de depot avec tous les paramètres requis ainsi que les méthodes.
//**************************************************************************************************************************

namespace TP1_OO
{
    public class PaquetDepot : Paquet
    {
        public delegate Carte DelegateCarte();


        //Constructeur
        public PaquetDepot() : base()
        {
        }

        public Carte VoirCarte()
        {
            int test = top;
            Carte carte = paquet[top];
            return carte;
        }

        public void DeposerCarte(Carte carte)
        {
            paquet[++top] = carte;
        }

    }
}

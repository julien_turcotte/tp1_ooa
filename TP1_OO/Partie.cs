﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//**************************************************************************************************************************************
//Partie.cs 										Auteur: Julien Turcotte, Philippe Baillargeon et Tommy Dumont
//
//Classe Partie pour création d'une partie avec tous les paramètres requis ainsi que les méthodes. Cette classe permet de Jouer au jeu.
//**************************************************************************************************************************************

namespace TP1_OO
{
    public class Partie
    {
        //Attributs
        private List<Joueur> listeJoueur;
        private PaquetDepot depot;
        private PaquetPioche pioche;
        private int indexJoueur;
        private int nbJoueurs;

        //Constructeur d'une partie
        public Partie(List<Joueur> listeJoueur, PaquetDepot depot, PaquetPioche pioche)
        {
            this.listeJoueur = listeJoueur;
            this.depot = depot;
            this.pioche = pioche;
            Random rand = new Random();
            indexJoueur = rand.Next(0, nbJoueurs);
            nbJoueurs = listeJoueur.Count;

            //Inscrire les abonnées
            for (int i = 0; i < nbJoueurs; i++)
                this.CardPlayed += listeJoueur[i].OnCardPlayed;
        }
        

        //-----------------------------------EVENT HANDLER------------------------------------------------//
        //Delegate (Event handler)
        public delegate void CardPlayedEventHandler(object source, PlayerEventArgs e, CardEventArgs s);
        //Event basé sur le delegate
        public event CardPlayedEventHandler CardPlayed;

        //Notifier les abonnés
        protected virtual void OnCardPlayed(Joueur j, Carte c)
        {
            if (CardPlayed != null)
                CardPlayed(this, new PlayerEventArgs() { Joueur = j}, new CardEventArgs() {Carte = c});
        }

        
        //regroupement des methodes pour initialiser une partie
        public void StartPartie()
        {
            //Distribue les cartes aux joueurs.
            pioche.DistribuerCartes(listeJoueur);

            //ReTourne une carte de départ sur le paquet de Dépot.
            depot.DeposerCarte(pioche.GetCarte());

            //Creer une methode Tour qui entre en argument le premier joueur.
            Jouer();
        }
       

        public void Jouer()
        {
            bool gameover = false;
            while (!gameover)
            {
                try
                {
                    Console.WriteLine("------------------------------------------------------------------------------");
                    Console.WriteLine("\nC'est le Tour à " + listeJoueur[indexJoueur].ToString() + "\n");
                    Console.WriteLine("La dernière carte jouée est: " + depot.VoirCarte().ToString());
                    Console.WriteLine("Voici votre paquet: \n" + listeJoueur[indexJoueur].GetMain());

                    var t = Task.Factory.StartNew(() =>
                    {
                        //Notifier tous les abonnées
                        OnCardPlayed(listeJoueur[indexJoueur], depot.VoirCarte());
                        Task.Delay(1000).Wait();

                        Tour();
                        if (pioche.GetNbCartes() == 0)
                        {
                            Carte[] cartes = depot.GetPaquet();
                            int top = depot.GetTop();
                            pioche.TransfertPaquet(cartes, top);
                            depot.Vider();
                            pioche.Brasser(10000);
                        }
                        gameover = listeJoueur[indexJoueur].Gagnant();
                    });

                    t.Wait();

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            Console.WriteLine("Le Gagnant est: {0}!\n", listeJoueur[indexJoueur].ToString());
        }

        //Gere les Tours et decide qui doit Jouer.
        public int Tour()
        {
            indexJoueur++;
            if (indexJoueur >= listeJoueur.Count)
                indexJoueur = 0;

            return indexJoueur;
        }

    }
}